import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HW_1Test {


    DesiredCapabilities capabilities = DesiredCapabilities.chrome();
    ChromeOptions chromeOptions = new ChromeOptions();

    @Test
    public void task_1Test() {


        WebDriver webDriver = new ChromeDriver(chromeOptions);
        webDriver.manage().window().maximize();

        try {

            webDriver.get("https://www.google.com");
            WebElement searchFieldGoogle = webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1) > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input"));
            searchFieldGoogle.click();
            searchFieldGoogle.sendKeys("Selenium");
            WebElement pushSearchButton = webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1) > div.A8SBwf > div.FPdoLc.tfB0Bf > center > input.gNO89b"));
            pushSearchButton.click();
            WebElement pushSeleniumURLField = webDriver.findElement(By.cssSelector("#rso > div > div:nth-child(1) > div > div.yuRUbf > a > h3"));
            pushSeleniumURLField.click();
            WebElement gettingStarted = webDriver.findElement(By.xpath("/html/body/section[2]/h2"));
            String expected = "Getting Started";
            assertEquals(expected, gettingStarted.getText());
            String expectedREsult = "https://www.selenium.dev/";
            assertEquals(expectedREsult, webDriver.getCurrentUrl());
        } finally {
            webDriver.close();
        }
    }

    @Test
    public void task_2Test() {

        chromeOptions.addArguments("incognito");
        chromeOptions.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        WebDriver webDriver = new ChromeDriver(chromeOptions);
        webDriver.manage().window().maximize();
        try {
            webDriver.get("https://mail.ru/");

            WebDriverWait wait = new WebDriverWait(webDriver, 60);
            WebElement loginField = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input"));
            loginField.click();
            loginField.sendKeys("seleniumexample@mail.ru");
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input")));

            WebElement clickPasswordButton = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y"));
            clickPasswordButton.click();
            webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            WebElement passField = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input"));
            passField.click();
            passField.sendKeys("Adl23oop*43");
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input")));

            WebElement voytiButton = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.second-button.svelte-1eyrl7y"));
            voytiButton.click();

            WebElement panelMail = webDriver.findElement(By.xpath("//*[@id=\"app-canvas\"]/div/div[1]/div[1]/div/div[1]"));
            assertTrue(true, String.valueOf(panelMail.isDisplayed()));

            List<WebElement> leftPanelBar = webDriver.findElements(By.cssSelector("#sideBarContent > div > nav > a.nav__item"));
            assertEquals(8, leftPanelBar.size());


        } finally {
            webDriver.close();
        }

    }

    @Test
    public void task_3Test() {


        chromeOptions.addArguments("incognito");
        chromeOptions.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        WebDriver webDriver = new ChromeDriver(chromeOptions);
        webDriver.manage().window().maximize();

        try {
            webDriver.get("https://mail.ru/");
            WebDriverWait wait = new WebDriverWait(webDriver, 60);
            WebElement loginField = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input"));
            loginField.click();
            loginField.sendKeys("asdfasd879879879@mail.ru");
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input")));

            WebElement clickPasswordButton = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y"));
            clickPasswordButton.click();

            webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            WebElement getErrorMessages = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.error.svelte-1eyrl7y"));
            String expected = getErrorMessages.getText();
            assertTrue(getErrorMessages.isDisplayed());
            assertEquals(expected, String.valueOf(getErrorMessages.getText()));


        } finally {
            webDriver.close();
        }
    }

    @Test
    public void task_4Test() {

        chromeOptions.addArguments("incognito");
        chromeOptions.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        WebDriver webDriver = new ChromeDriver(chromeOptions);
        webDriver.manage().window().maximize();

        try {
            webDriver.get("https://mail.ru/");

            WebDriverWait wait = new WebDriverWait(webDriver, 60);
            WebElement loginField = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input"));
            loginField.click();
            loginField.sendKeys("seleniumexample@mail.ru");
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input")));

            WebElement clickPasswordButton = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y"));
            clickPasswordButton.click();
            wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y")));
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            WebElement passField = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input"));
            passField.click();

            passField.sendKeys("Adl23oop*43");
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input")));

            WebElement voytiButton = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.second-button.svelte-1eyrl7y"));
            voytiButton.click();
            wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.second-button.svelte-1eyrl7y")));

            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            WebElement napisatPismoButton = webDriver.findElement(By.xpath("//*[@id=\"app-canvas\"]/div/div[1]/div[1]/div/div[2]/span/div[1]/div[1]/div/div/div/div[1]/div/div/a/span"));
            napisatPismoButton.click();

            webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            WebElement komuField = webDriver.findElement(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_fixed > div > div.compose-app__compose > div.container--rp3CE.compressed--2kH7H > div.scrollview--SiHhk.scrollview_main--3Vfg9.scrollview_mainCompressed--2IhQY > div.head_container--3W05z > div > div > div.wrap--2sfxq.compressed--23JBG > div > div.contacts--1ofjA > div > div > label > div > div > input"));
            komuField.click();

            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            komuField.sendKeys("farruxalighumbatov@gmail.com");
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_fixed > div > div.compose-app__compose > div.container--rp3CE.compressed--2kH7H > div.scrollview--SiHhk.scrollview_main--3Vfg9.scrollview_mainCompressed--2IhQY > div.head_container--3W05z > div > div > div.wrap--2sfxq.compressed--23JBG > div > div.contacts--1ofjA > div > div > label > div > div > input")));

            WebElement themField = webDriver.findElement(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_fixed > div > div.compose-app__compose > div.container--rp3CE.compressed--2kH7H > div.scrollview--SiHhk.scrollview_main--3Vfg9.scrollview_mainCompressed--2IhQY > div.subject__container--HWnat > div.subject__wrapper--2mk6m > div.container--3QXHv.compressed--2KOQl > div > input"));
            themField.click();
            themField.sendKeys("Hi!");
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_fixed > div > div.compose-app__compose > div.container--rp3CE.compressed--2kH7H > div.scrollview--SiHhk.scrollview_main--3Vfg9.scrollview_mainCompressed--2IhQY > div.subject__container--HWnat > div.subject__wrapper--2mk6m > div.container--3QXHv.compressed--2KOQl > div > input")));

            WebElement prikrepitFayl = webDriver.findElement(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_fixed > div > div.compose-app__compose > div.container--rp3CE.compressed--2kH7H > div.scrollview--SiHhk.scrollview_main--3Vfg9.scrollview_mainCompressed--2IhQY > div.attach_container--2NlSY > div > div > div > button:nth-child(2)"));
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            prikrepitFayl.sendKeys("src\\test\\resources\\some\\path\\resources\\hi.txt");
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            WebElement otpravitButton = webDriver.findElement(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_fixed > div > div.compose-app__footer > div.compose-app__buttons > span.button2.button2_base.button2_primary.button2_compact.button2_hover-support.js-shortcut > span > span"));
            otpravitButton.click();

            WebElement otpravitLastControl = webDriver.findElement(By.cssSelector("body > div.overlay--2THpd > div > div > div.c1110 > button.c1180.c1155.c1191.c1166.c1199.c1175"));
            otpravitLastControl.click();
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            WebElement sending = webDriver.findElement(By.xpath("/html/body/div[5]/div/div[1]/div[1]/div/div[2]/span/div[1]/div[1]/div/div/div/div[2]/div/div/div/div/nav/a[5]/div/div[2]/div"));
            sending.click();
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            WebElement isMailSending = webDriver.findElement(By.xpath("/html/body/div[9]/div/div/div[2]/div[2]/div/div/div[2]/a"));

            assertTrue(true, String.valueOf(isMailSending.isDisplayed()));


        } finally {
            webDriver.close();

        }

    }

    @Test
    public void task_51Test() {

        chromeOptions.addArguments("incognito");
        chromeOptions.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        WebDriver webDriver = new ChromeDriver(chromeOptions);
        webDriver.manage().window().maximize();
        try {
            WebDriverWait wait = new WebDriverWait(webDriver, 60);
            Actions action = new Actions(webDriver);
            webDriver.get("https://www.etsy.com/ ");
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            WebElement clothingAndShoes = webDriver.findElement(By.cssSelector("#desktop-category-nav > div.wt-bg-white.wt-hide-xs.wt-show-lg.wt-text-caption.wt-position-relative.wt-z-index-4.v2-toolkit-cat-nav-tab-bar > div > ul > li:nth-child(2)"));


            action.moveToElement(clothingAndShoes).perform();

            WebElement clothingAndShoesMan = webDriver.findElement(By.xpath("/html/body/div[4]/div[2]/div/div[2]/div/div/aside/ul/li[2]/span"));

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[4]/div[2]/div/div[2]/div/div/aside/ul/li[2]/span")));
            action.moveToElement(clothingAndShoesMan).perform();

            WebElement clothingAndShoesMan_sShoes = webDriver.findElement(By.cssSelector("#catnav-l4-11109"));
            action.moveToElement(clothingAndShoesMan_sShoes).click(clothingAndShoesMan_sShoes).perform();
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            WebElement discountCheckBox = webDriver.findElement(By.cssSelector("#search-filter-reset-form > div:nth-child(2) > fieldset > div > div > div:nth-child(2) > div > a > label"));
            discountCheckBox.click();
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            WebElement calculateDiscount = webDriver.findElement(By.cssSelector("#content > div > div.content.bg-white.col-md-12.pl-xs-1.pr-xs-0.pr-md-1.pl-lg-0.pr-lg-0.wt-bb-xs-1 > div > div.mt-xs-2.pl-xs-1.pl-md-4.pl-lg-6.pr-xs-1.pr-md-4.pr-lg-6 > div > span"));
            String discountsElementCalculating = calculateDiscount.getText();

            assertTrue(calculateDiscount.isDisplayed());
            String expected = "1730";
            String actual = discountsElementCalculating.substring(1, 7).replaceAll("\\s+", "");
            assertEquals(actual, expected); // actual value is dinamic value in order
            // to I can check this operation...

        } finally {
            webDriver.close();
        }
    }

    @Test
    public void task_52Test() {
        chromeOptions.addArguments("incognito");
        chromeOptions.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        WebDriver webDriver = new ChromeDriver(chromeOptions);
        webDriver.manage().window().maximize();
        try {
            WebDriverWait wait = new WebDriverWait(webDriver, 60);
            Actions action = new Actions(webDriver);
            webDriver.get("https://www.etsy.com/ ");
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            WebElement clothingAndShoes = webDriver.findElement(By.cssSelector("#desktop-category-nav > div.wt-bg-white.wt-hide-xs.wt-show-lg.wt-text-caption.wt-position-relative.wt-z-index-4.v2-toolkit-cat-nav-tab-bar > div > ul > li:nth-child(2)"));


            action.moveToElement(clothingAndShoes).perform();

            WebElement clothingAndShoesMan = webDriver.findElement(By.xpath("/html/body/div[4]/div[2]/div/div[2]/div/div/aside/ul/li[2]/span"));

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[4]/div[2]/div/div[2]/div/div/aside/ul/li[2]/span")));
            action.moveToElement(clothingAndShoesMan).perform();

            WebElement clothingAndShoesMan_sShoes = webDriver.findElement(By.cssSelector("#catnav-l3-10936"));
            action.moveToElement(clothingAndShoesMan_sShoes).click(clothingAndShoesMan_sShoes).perform();
            webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            WebElement calculateDiscount = webDriver.findElement(By.cssSelector("#content > div > div.content.bg-white.col-md-12.pl-xs-1.pr-xs-0.pr-md-1.pl-lg-0.pr-lg-0.wt-bb-xs-1 > div > div.mt-xs-2.pl-xs-1.pl-md-4.pl-lg-6.pr-xs-1.pr-md-4.pr-lg-6 > div > span"));
            String discountsElementCalculating = calculateDiscount.getText();

            assertTrue(calculateDiscount.isDisplayed());
            String expected = "4154392";
            String actual = discountsElementCalculating.substring(1, 11).replaceAll("\\s+", "");
            assertEquals(actual, expected); // actual value is dinamic value in order
            // to I can check this operation...

        } finally {
            webDriver.close();
        }
    }

}